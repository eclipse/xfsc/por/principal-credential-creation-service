module gitlab.com/gaia-x/data-infrastructure-federation-services/por/principal-credential-creation-service

go 1.19

require (
	github.com/golang-jwt/jwt/v4 v4.4.2 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e // indirect
	github.com/toorop/go-dkim v0.0.0-20201103131630-e1cd1a0a5208 // indirect
	github.com/xhit/go-simple-mail/v2 v2.13.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.23.0 // indirect
)
